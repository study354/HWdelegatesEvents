﻿using HWdelegatesEvents;

#region Задание номер 1

var maxFromClass = new List<FileFounder> { new FileFounder(), new FileFounder() }.GetMax<FileFounder>((x) => { return (float)x?.ToString().Length; });
Console.WriteLine("{0} is max.", maxFromClass);

#endregion

#region Задание номер 2 - 5

Console.WriteLine($"Finder files in direcory.{Environment.NewLine}Input path:");
string? path = Console.ReadLine();

CancellationTokenSource cancelTokenSource = new CancellationTokenSource();

FileFounder f = new FileFounder();
f.FileFound += GetFile;

try
{
    Console.WriteLine($"Start finder.{Environment.NewLine}Press ESC for exit or any key for get file.");
    f.FindFiles(path, cancelTokenSource.Token);
}
catch (Exception ex) when (ex is IOException || ex is ArgumentNullException)
{
    Console.WriteLine("Input not correct path. Press any key for exit...");
    Console.ReadKey();
}


void GetFile(object? sender, FileArgs e)
{
    if (Console.ReadKey().Key == ConsoleKey.Escape)
    {
        cancelTokenSource.Cancel();
        return;
    }
    Console.WriteLine("FullName new file - {0}.", e.FileName);
}

#endregion