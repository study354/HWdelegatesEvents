﻿namespace HWdelegatesEvents
{
    public class FileFounder
    {
        public event EventHandler<FileArgs>? FileFound;

        public void FindFiles(string? path, CancellationToken token)
        {
            if (string.IsNullOrEmpty(path)) throw new ArgumentNullException(nameof(path));

            DirectoryInfo? dir = new DirectoryInfo(path);
            if (dir == null) throw new NullReferenceException(nameof(DirectoryInfo));

            foreach (FileInfo? file in dir.GetFiles())
            {
                OnGetNextFile(new FileArgs { FileName = file.FullName });
                if (token.IsCancellationRequested)
                {
                    return;
                }
            }
            Console.WriteLine("Find is over.");
        }

        protected virtual void OnGetNextFile(FileArgs e)
        {
            FileFound?.Invoke(this, e);
        }
    }

    public class FileArgs : EventArgs
    {
        public string? FileName { get; set; }
    }
}
