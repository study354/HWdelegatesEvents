﻿using System.Collections;

namespace HWdelegatesEvents
{
    internal static class IEnumerableExtention
    {
        public static T GetMax<T>(this IEnumerable e, Func<T, float> getParameter) where T : class
        {
            T res = null;
            float max = float.MinValue;
            float current;
            foreach (T item in e)
            {
                current = getParameter(item);
                if (current > max)
                {
                    max = current;
                    res = item;
                }
            }
            return res;
        }



    }
}
